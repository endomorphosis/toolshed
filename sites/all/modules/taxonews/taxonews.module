<?php
/**
 * A module to display blocks selected by terms in a taxonomy vocabulary,
 * as long as they are newer than a set limit
 *
 * The module defines one block for each term in the vocabulary.
 *
 * Settings allows the administrator to choose
 *   - the vocabulary to be used to select nodes
 *   - the number of days nodes are considered for display
 *
 * Blocks without matching nodes do not appear.
 *
 * @author     Frederic G. MARAND
 * @version    CVS: $Id: taxonews.module,v 1.10.8.10 2008/10/13 09:09:10 fgm Exp $
 * @copyright  2005-2008 Ouest Systèmes Informatiques (OSI)
 * @license    http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
 * @link       http://drupal.org/project/taxonews
 * @since      Version 1.1
 */

$_taxonewsErrorReporting = error_reporting(E_ALL | E_STRICT);

class Taxonews
  {
  const VERSION = '$Id: taxonews.module,v 1.10.8.10 2008/10/13 09:09:10 fgm Exp $';
  const PATH_SETTINGS = 'admin/build/settings/taxonews';

  /**
   * Names of persistent variables
   */
  const VAR_LIFETIME       = 'taxonews_lifetime';
  const VAR_MAX_ROWS       = 'taxonews_max_rows';
  const VAR_SHOW_NAME      = 'taxonews_show_name';
  const VAR_SHOW_ARCHIVE   = 'taxonews_show_archive';
  const VAR_SHOW_EMPTY     = 'taxonews_show_empty';
  const VAR_EMPTY_MESSAGES = 'taxonews_empty_messages';
  const VAR_FEED           = 'taxonews_feed';
  const VAR_PONDERATED     = 'taxonews_ponderated';
  const VAR_VOCABULARY     = 'taxonews_vid';

  /**
   * Default values for persistent variables
   *
   */
  const DEF_LIFETIME       = 30;
  const DEF_MAX_ROWS       =  5;
  const DEF_VOCABULARY     =  0;

  /**
   * Implement the former hook_settings
   *
   * @return array settings form
   */
  static function adminSettings()
    {
    $sq = 'SELECT v.vid, v.name, v.description '
        . 'FROM {vocabulary} v '
        . 'ORDER BY 2, 1 ' ;
    $q = db_query($sq);
    $vocabularies = array();
    while ($o = db_fetch_object($q))
      {
      $vocabularies["$o->vid"] = "$o->name - $o->description" ;
      }

    $form[self::VAR_VOCABULARY] = array
      (
      '#type'             => 'select',
      '#title'            => t('Vocabularies'),
      '#default_value'    => variable_get(self::VAR_VOCABULARY, self::DEF_VOCABULARY),
      '#options'          => $vocabularies,
      '#description'      => t('Vocabularies to be used for the generation of blocks. A block will be defined for each term in each vocabulary selected here.'),
      '#required'         => TRUE,
      '#size'             => count($vocabularies),
      '#multiple'         => TRUE,
      );

    $form[self::VAR_PONDERATED] = array
      (
      '#type'             => 'select',
      '#title'            => t('Ponderate sorting by'),
      '#options'          => array
        (
        0 => t('Creation date'),
        ),
      );
    if (module_exists('statistics'))
      {
      $form[self::VAR_PONDERATED]['#options'] += array
        (
        1 => t('Total views'),
        2 => t('Daily views'),
        );
      $form[self::VAR_PONDERATED] += array
        (
        '#default_value'  => variable_get(self::VAR_PONDERATED, 0),
        '#description'    => t('Sort displayed news by descending creation date, possibly ponderated by total number of views and daily views. This ponderation only affects sorting, not news selection, which are always selected by creation date.'),
        );
      }
    else
      {
      $form[self::VAR_PONDERATED] += array
        (
        '#default_value'  => 0,
        '#description'    => t('Sort displayed news by descending creation date. Additional sortings will be available if you !enable. It is currently disabled.',
          array('!enable' => l(t('enable statistics.module'), 'admin/build/modules'))),
        );
      }

    $form[self::VAR_LIFETIME] = array
      (
      '#type'             => 'textfield',
      '#title'            => t('Lifetime of news before expiration'),
      '#default_value'    => variable_get(self::VAR_LIFETIME, self::DEF_LIFETIME),
      '#size'             => 3,
      '#max_length'       => 3,
      '#description'      => t('The number of days a news item remains displayed in blocks. If set to 0, news never expire.'),
      '#required'         => TRUE,
      )  ;
    $form[self::VAR_MAX_ROWS] = array
      (
      '#type'             => 'textfield',
      '#title'            => t('Maximum number of rows per block'),
      '#default_value'    => variable_get(self::VAR_MAX_ROWS, self::DEF_MAX_ROWS),
      '#size'             => 3,
      '#max_length'       => 3,
      '#description'      => t('The maximum number of rows that the builtin theme will display in a block.'),
      '#required'         => TRUE,
      )  ;
    $form[self::VAR_FEED] = array
      (
      '#type'            => 'checkbox',
      '#title'           => t('Display RSS feed icon on taxonews blocks'),
      '#default_value'   => variable_get(self::VAR_FEED, TRUE),
      '#description'     => t('This setting allows taxonews to display a RSS feed icon on each non empty block. Default value is enabled.'),
      );
    $form[self::VAR_SHOW_NAME] = array
      (
      '#type'            => 'checkbox',
      '#title'           => t('Prepend taxonews module name in block list'),
      '#default_value'   => variable_get(self::VAR_SHOW_NAME, TRUE),
      '#description'     => t('This allows the modules to appear grouped on the block list at Administer/Blocks, to avoid cluttering.'),
      );
    $form[self::VAR_SHOW_ARCHIVE] = array
      (
      '#type'            => 'checkbox',
      '#title'           => t('Include "Archive" link to older articles in block'),
      '#default_value'   => variable_get(self::VAR_SHOW_ARCHIVE, TRUE),
      '#description'     => t('If articles matching the term exist, but cannot be placed in the block, for instance if they are too old, add an "Archive" link at the end of the block. The link will not be shown if no matching article exists.'),
      );
    $form[self::VAR_SHOW_EMPTY] = array
      (
      '#type'            => 'checkbox',
      '#title'           => t('Include blocks with currently no matching nodes in the blocks list'),
      '#default_value'   => variable_get(self::VAR_SHOW_EMPTY, TRUE),
      );
    $form['advanced'] = array
      (
      '#type'            => 'fieldset',
      '#collapsible'     => TRUE,
      '#collapsed'       => TRUE,
      '#title'           => t('Advanced settings'),
      );
    $form['advanced'][self::VERSION] = array(
      '#value'           => t('<p>This site is running taxonews.module version: @version.</p>',
        array('@version' => self::VERSION)),
      );

    return system_settings_form($form);
    }

  /**
   * Is there any published data matching that term ?
   *
   * @param int $tid
   * @param array $nids Array of displayed nodes, to ignore
   * @return boolean
   *
   * The function expects the nids of the already displayed nodes
   * to be the keys of the $items array, so it can ignore them
   */
  static function archiveExists($tid, $items = array())
    {
    $sq = 'SELECT COUNT(tn.nid) '
        . 'FROM {term_node} tn '
        . 'INNER JOIN {node} n ON tn.vid = n.vid '
        . 'WHERE tn.tid = %d AND n.status = 1 ';

    if (count($items))
      {
      $sq .= 'AND tn.nid NOT IN (' . db_placeholders(array_keys($items)) . ') ';
      }
    $q = db_query_range($sq, $tid, 0, 1);
    $ret = db_result($q);
    $ret = $ret > 0;
    return $ret;
    }

  /**
   * Configure the taxonews block identified by $delta
   *
   * @param mixed $delta
   * @return array
   */
  static function blockConfigure($delta)
    {
    $description = t('By default, blocks without matching content are not displayed. This setting allows you to force a static content.');

    $form [self::VAR_EMPTY_MESSAGES] = array
      (
      '#title'         => t('Text to be displayed if block has no matching content'),
      '#type'          => 'textfield',
      '#default_value' => self::getEmptyMessages($delta),
      '#size'          => 60,
      '#max_length'    => 60,
      '#description'   => $description,
      );

    return $form;
    }

  /**
   * Generate block list for hook_block
   *
   * @return array
   * @see taxonews_block
   */
  static function blockList()
    {
    $arBlocks = array() ;
    $arTerms = self::getTerms();
    if (empty($arTerms))
      {
      $modulePath = dirname(drupal_get_filename('module', 'taxonews'));
      drupal_set_message(
        t('WARNING: You will not be able to configure taxonews blocks until you !configure and !define in the taxonews vocabularies. You might want to refer to !install.',
          array(
            '!configure' => l(t('configure taxonews'), self::PATH_SETTINGS),
            '!define'    => l(t('define terms'), 'admin/content/taxonomy'),
            '!install'   => l('INSTALL.txt', "$modulePath/INSTALL.txt")
            )
          ),
        'error');
      unset($modulePath);
      return $arBlocks; // No need to run the following code: there is no block
      }

    $prefix = variable_get(self::VAR_SHOW_NAME, TRUE) ? 'Taxonews/' : '' ;
    $showEmpty = variable_get(self::VAR_SHOW_EMPTY, TRUE);
    $sq = 'SELECT tn.tid, COUNT(tn.nid) AS cnt '
        . 'FROM {term_node} tn '
        . 'INNER JOIN {node} n ON tn.vid = n.vid '
        . 'WHERE n.status = 1 AND tn.tid IN (' . db_placeholders($arTerms) . ') '
        . 'GROUP BY 1 ';
    $arCounts = array();
    $q = db_query($sq, array_keys($arTerms));
    while ($o = db_fetch_object($q))
      {
      $arCounts[$o->tid] = $o->cnt;
      }
    foreach ($arTerms as $tid => $term)
      {
      if (($arCounts[$tid] == 0) && !$showEmpty)
        {
        continue; // Do not display the block, do not increment the counter
        }
      $arBlocks[$tid] = array() ;
      $arBlocks[$tid]['info'] = "$prefix$term->vocabulary_name/$term->name" ;
      $arBlocks[$tid]['cache'] = BLOCK_CACHE_GLOBAL;
      }

    return $arBlocks ;
    }

  /**
   * Save the configuration (i.e. the text) of the selected block
   *
   * @param mixed $delta Usually an int
   * @param $edit The edit form fields
   * @return void
   * @see hook_block()
   */
  static function blockSave($delta, $edit)
    {
    $arEmptyMessages = self::getEmptyMessages();
    $arEmptyMessages[$delta] = $edit[self::VAR_EMPTY_MESSAGES];
    variable_set(self::VAR_EMPTY_MESSAGES, $arEmptyMessages);
    }

  /**
   * Generate block contents for the passed-in delta
   *
   * Note: 86400 = 24*60*60 = seconds in one day
   *
   * @param mixed $delta Drupal block delta
   * @return string HTML
   */
  static function blockView($delta)
    {
    $arTerms = self::getTerms() ;

    /**
     * ponderation must only be taken into account if statistics module exists
     * because the module may have been online, allowing ponderation to be set,
     * then removed, causing stats to no longer be updated
     */
    $ponderation = module_exists('statistics')
      ? variable_get(self::VAR_PONDERATED, 0)
      : 0;

    /**
     * There's a small trick for case 0|default: n.created is not a views count, but can
     * be used exactly like one: more recent nodes will have a higher value in
     * this field, so we can sort on it for display just like we sort on actual
     * view counts otherwise. That way we only have one sort rule for all three
     * different cases.
     */
    switch ($ponderation)
      {
      case 1: // by total views
        $sq = 'SELECT n.nid, n.title, td.name, '
            . '  nc.totalcount AS views '
            . 'FROM {node} n '
            . '  INNER JOIN {term_node} tn ON n.vid = tn.vid '
            . '  INNER JOIN {term_data} td ON tn.tid = td.tid '
            . '  LEFT JOIN {node_counter} nc on n.nid = nc.nid ' // Some nodes may never have been counted yet
            . '  /* ignore current time: %d */ ';
        break;

      case 2:  // by daily views since creation.
        $sq = 'SELECT n.nid, n.title, td.name, '
            . '  nc.totalcount*86400/(%d-n.created) AS views '
            . 'FROM {node} n '
            . '  INNER JOIN {term_node} tn ON n.vid = tn.vid '
            . '  INNER JOIN {term_data} td ON tn.tid = td.tid '
            . '  LEFT JOIN {node_counter} nc on n.nid = nc.nid ';  // Some nodes may never have been counted yet
        break;

      case 0: // not ponderated
      default: // ignore invalid values
        $sq = 'SELECT n.nid, n.title, td.name, '
            . '  n.created AS views '
            . 'FROM {node} n '
            . '  INNER JOIN {term_node} tn ON n.vid = tn.vid '
            . '  INNER JOIN {term_data} td ON tn.tid = td.tid '
            . '  /* ignore current time: %d */ ';
        break;
      }

    $sq .=    'WHERE td.tid = %d '
            . '  AND (n.created > %d) '
            . '  AND (n.status = 1) '
            . '  ORDER BY n.created DESC, n.changed DESC ';

    $lifetime = variable_get(self::VAR_LIFETIME, self::DEF_LIFETIME);
    $creation = $lifetime
      ? time() - 86400 * $lifetime
      : 0;

    $ret   = array();
    $items = array();
    if ($q = db_query_range ($sq,
      time(), $arTerms[$delta]->tid, $creation,               // current time, tid, lifetime
      0, variable_get(self::VAR_MAX_ROWS, self::DEF_MAX_ROWS) // range
      ))
      {
      while ($o = db_fetch_object ($q))
        {
        $items[$o->nid] = l($o->title, 'node/' . $o->nid) ;

        // $o->name is undefined outside the loop, and re-initializing it for every
        // value in the loop seems to actually be the cheapest way of keeping it, although
        // its value is the same for all rows in the result set
        $ret['subject'] = $o->name ;
        }
      krsort($items); // sort descending on views
      }
    if (count($items) == 0)
      {
      $items = NULL;
      $term = taxonomy_get_term($arTerms[$delta]->tid);
      $ret['subject'] = $term->name;
      }

    $ret['content'] = theme('taxonews_block_view', $delta, $items, $arTerms[$delta]->tid);
    return $ret;
    }

  /**
   * Return the list of messages to be issued when an empty taxonews blocks is
   * to be built
   *
   * @param mixed $delta
   * @return array
   */
  static function getEmptyMessages($delta = NULL)
    {
    static $arEmptyMessages;

    if (empty($arEmptyMessages))
      {
      $arEmptyMessages = variable_get(self::VAR_EMPTY_MESSAGES, array());
      }

    $ret = isset($delta)
      ? (array_key_exists($delta, $arEmptyMessages) ? $arEmptyMessages[$delta] : NULL)
      : $arEmptyMessages;

    return $ret;
    }

  /**
   * Query for terms in the vocabularies selected in settings
   *
   * @return array [tid, name]
   */
  static function getTerms()
    {
    static $arTerms = array();

    if (empty($arTerms))
      {
      $arVids = variable_get(self::VAR_VOCABULARY, array(self::DEF_VOCABULARY));
      $sq = 'SELECT td.tid, td.name, '
          . '  v.name AS vocabulary_name '
          . 'FROM {term_data} td '
          . '  INNER JOIN {vocabulary} v ON td.vid = v.vid '
          . 'WHERE td.vid IN (' . db_placeholders($arVids) . ') '
          . 'ORDER BY 2, 1 ';
      $q = db_query($sq, $arVids);
      while ($o = db_fetch_object($q))
        {
        $arTerms[$o->tid] = $o;
        }
      }

    return $arTerms;
    }
  } // end of class Taxonews

/**
 * Until a more generic way to use class methods as callbacks is available
 * or included in core
 *
 * @param array $formState New Drupal 6 data. Unused
 * @param string $method
 * @return mixed
 */
function _taxonews_form_rerouter($formState, $method)
  {
  return Taxonews::$method();
  }

/**
 * -----------------------------------------------------------------------------
 * Drupal hooks below
 * -----------------------------------------------------------------------------
 */

/**
 * Implement hook_help
 *
 * @param string $path
 * @param string $arg
 * @return string
 */
function taxonews_help($path, $arg)
  {
  $ret = NULL;
  $help = t('<p>Publish node teasers into blocks based on a taxonomy vocabulary.</p>') ;
  switch ($path)
    {
    case 'admin/help#taxonews':
      $ret = $help
      . t('<p>This module creates blocks containing a selection of node titles, linking to the node themselves. Blocks are created for each taxonomy term in the vocabularies enabled for block generation.</p>')
      . t('<p>Settings allow the administrator to choose the age limit for nodes included in the blocks, as well as various settings to handle special cases like empty blocks.</p>')
      . t('<p>By default, nodes are displayed in reverse chronological order. If statistics.module is enabled and node counts are active, nodes can also be displayed by descending order of total views or daily views since their creation. The node selection process by creation date is not affected by these sorting rules.</p>');
      break;
    case 'admin/modules#description':
      $ret = $help;
      break;
    }
  return $ret;
  }

/**
 * implement hook_block
 *
 * @param string $op list|view|configure|save
 * @param int $delta
 * @param array $edit
 * @return mixed either void or HTML for blocks/block list
 * @see Taxonews::blockList
 * @see Taxonews::blockView
 */
function taxonews_block($op = 'list', $delta = 0, $edit = array())
  {
  switch ($op)
    {
    case 'list':      $ret = Taxonews::blockList(); break;
    case 'view':      $ret = Taxonews::blockView($delta); break;
    case 'configure': $ret = Taxonews::blockConfigure($delta); break;
    case 'save':      $ret = Taxonews::blockSave($delta, $edit); break;
    default:          $ret = NULL;
    }
  return $ret;
  }

/**
 * Implement the modified Drupal 6 hook_menu
 *
 * @return array
 */
function taxonews_menu()
  {
  $items[Taxonews::PATH_SETTINGS] = array
    (
    'title'            => 'Taxonews',
    'description'      => 'Define the various parameters used by the taxonews module',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('_taxonews_form_rerouter', 'adminSettings'),
    'access arguments' => array('administer site configuration'),
    'type'             => MENU_NORMAL_ITEM,
    );

  return $items;
  }

/**
 * Implement the new Drupal 6 hook_theme
 *
 * @return array
 */
function taxonews_theme($existing = array(), $type = 'module', $theme = 'taxonews')
  {
  $ret = array
    (
    'taxonews_block_view' => array
      (
      'arguments' => array
         (
         'delta' => NULL,
         'arItems' => array(),
         'tid' => 0,
         ),
      ),
    );
  return $ret;
  }

/**
 * Themeable function for TN blocks
 *
 * The default theme won't link to the "archive" page if there are no
 * nodes in addition to the ones already listed. If a site-specific theme
 * want to display the link anyway, pass null instead of $items to
 * Taxonews::archiveExists to check for the page contents. Note that in most
 * normal situations, it will exist and contain at least the nodes already listed
 * in the block.
 *
 * @param mixed $delta the block delta, usually integer but now always
 * @param array $items A possibly empty array of links
 * @param int $tid The tid for the taxonomy term used to build the block
 * @see Taxonews::archiveExists
 *
 */
function theme_taxonews_block_view($delta, $arItems = array(), $tid)
  {
  $ret = empty($arItems)
    ? Taxonews::getEmptyMessages($delta)
    : theme('item_list', $arItems);

  if (!empty($ret) && Taxonews::archiveExists($tid, $arItems))
    {
    if (variable_get(Taxonews::VAR_SHOW_ARCHIVE, TRUE))
      {
      $ret .= '<span class="more-link">'
           .  l(t('Archive'), "taxonomy/term/$tid", array('absolute' => TRUE))
           .  '</span> ' ;
      }
    }

  if (!empty($ret) && variable_get(Taxonews::VAR_FEED, TRUE))
    {
    $ret .= theme('feed_icon', url("taxonomy/term/$tid/0/feed"), t('RSS feed'));
    }

  $ret = empty($ret)
    ? NULL
    : "<div class=\"block-taxonews\">$ret</div>\n";

  return $ret;
  }

error_reporting($_taxonewsErrorReporting);
unset($_taxonewsErrorReporting);
